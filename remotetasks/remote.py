from common.model.task import ModelTask


class RemoteTask(ModelTask):
    """
    A template class for defining tasks that are executed remotely via API calls
    """