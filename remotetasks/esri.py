from remote import RemoteTask


class ArcServerTask(RemoteTask):
    """
    A class defining a remote tasks using ESRI's ArcGis Server
    """