from distutils.core import setup
setup(
    name='modeltasksremote',
    packages=['remotetasks'],
    version='0.1',
    license='MIT',
    description='Adds remote tasks to the model tasks-package',
    author='Thomas Wanderer',
    author_email='geotom@gisconsulting.de',
    url='https://gitlab.com/geotom/model-tasks-remote',
    download_url='https://gitlab.com/geotom/model-tasks-remote/release/modeltasksremote_01.tar.gz',
    keywords=['Tasks', 'Model', 'DAG', 'Graph', 'Model', 'Processing', 'Remote', 'WPS', 'ESRI'],
    install_requires=[
        'model-tasks'
    ],
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'Intended Audience :: Science/Research',
        'Topic :: Software Development :: Processing Tools',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.8',
    ],
)